﻿using System;

namespace NextBiggerTask
{
#pragma warning disable
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer and null if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int? NextBiggerThan(int number)
        {
            if(number == int.MaxValue) 
                return null;

            if (number < 0)
            {
                throw new ArgumentException("Source number must be greater than or equal to 0.", nameof(number));
            }

            string numberString = number.ToString();
            char[] digits = numberString.ToCharArray();

            // Find the first digit from the right that is smaller than the digit to its right
            int i;
            for (i = digits.Length - 2; i >= 0; i--)
            {
                if (digits[i] < digits[i + 1])
                {
                    break;
                }
            }

            // If no such digit is found, it is not possible to form a larger number
            if (i < 0)
            {
                return null;
            }

            // Find the smallest digit to the right of digits[i] that is greater than digits[i]
            int j;
            for (j = digits.Length - 1; j > i; j--)
            {
                if (digits[j] > digits[i])
                {
                    break;
                }
            }

            // Swap digits[i] and digits[j]
            char temp = digits[i];
            digits[i] = digits[j];
            digits[j] = temp;

            // Sort the digits to the right of i in ascending order
            Array.Sort(digits, i + 1, digits.Length - i - 1);

            // Convert the modified digits back to an integer
            int? result = int.Parse(new string(digits));

            return result;
        }
    }

}
